package id.ac.ui.cs.advprog.tutorial5.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SoulTest {
    Soul soul;

    @BeforeEach
    public void setUp(){
        this.soul = new Soul();
        soul.setAge(20);
        soul.setGender("F");
        soul.setId(12);
        soul.setName("Dark soul");
        soul.setOccupation("wandering soul");
    }

    @Test
    public void testRetrieveId(){
        // TODO create test
        assertEquals(12,soul.getId());
    }

    @Test
    public void testRetrieveAge(){
        // TODO create test
        assertEquals(20,soul.getAge());
    }

    @Test
    public void testRetrieveGender(){
        // TODO create test
        assertEquals("F",soul.getGender());
    }

    @Test
    public void testRetrieveName(){
        // TODO create test
        assertEquals("Dark soul",soul.getName());
    }

    @Test
    public void testRetrieveOccupation(){
        // TODO create test
        assertEquals("wandering soul",soul.getOccupation());
    }

}
