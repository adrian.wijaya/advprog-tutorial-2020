package id.ac.ui.cs.advprog.tutorial5.service;


import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRespository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class SoulServiceImplTest {

    @Mock
    private SoulRespository soulRespository;

    @InjectMocks
    private SoulServiceImpl soulService;

    private Soul soul;

    @BeforeEach
    public void setUp() {
        this.soul = new Soul();
        this.soul.setName("Ryo wibu");
        this.soul.setOccupation("Student");
        this.soul.setAge(20);
        this.soul.setId(1);
        this.soul.setGender("M");
        soulService.register(soul);
    }

    @Test
    public void testMethodFindAll() {
        List<Soul> soulList = soulService.findAll();
        verify(soulRespository, times(1)).findAll();
    }

    @Test
    public void testMethodErase() {
        long id = 1;
        soulService.erase(id);
        verify(soulRespository, times(1)).deleteById(id);
    }

    @Test
    public void testMethodfindSoul() {
        long id = 1;
        Optional<Soul> opt = soulService.findSoul(id);
        verify(soulRespository, times(1)).findById(id);
    }

    @Test
    public void testMethodRegisterSoul() {
        Soul dummySoul = new Soul();
        soulService.register(dummySoul);
        verify(soulRespository,times(1)).save(soul);
    }


    @Test
    public void testMethodRewriteSoul() {
        Soul dummySoul = new Soul();
        soulService.rewrite(dummySoul);
        verify(soulRespository,times(1)).save(soul);
    }
}
