package id.ac.ui.cs.advprog.tutorial5.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = SoulController.class)
public class SoulControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SoulService soulService;

    static final MediaType MEDIA = new MediaType(
            MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8")
    );

    @Test
    public void testListAllSoul() throws Exception {
        mockMvc.perform(get("/soul"))
                .andExpect(status().isOk());
    }

    @Test
    public void testFindById() throws Exception {
        // Not Exist
        mockMvc.perform(get("/soul/1"))
                .andExpect(status().isBadRequest());


    }

    @Test
    public void testPostRequest() throws Exception {
        Soul soul = new Soul();
        soul.setAge(20);
        soul.setGender("F");
        soul.setId(1);
        soul.setName("Dark soul");
        soul.setOccupation("wandering soul");

        ObjectMapper map = new ObjectMapper();
        map.configure(SerializationFeature.WRAP_ROOT_VALUE,false);
        ObjectWriter writer = map.writer().withDefaultPrettyPrinter();
        String requestBody = writer.writeValueAsString(soul);

        mockMvc.perform(post("/soul")
                .contentType(MEDIA)
                .content(requestBody))
                .andExpect(status().isOk());
    }

    @Test
    public void testPutRequest() throws Exception {
        // Exist
        Soul soul = new Soul();
        soul.setAge(20);
        soul.setGender("F");
        soul.setId(2);
        soul.setName("Dark soul");
        soul.setOccupation("wandering soul");

        ObjectMapper map = new ObjectMapper();
        map.configure(SerializationFeature.WRAP_ROOT_VALUE,false);
        ObjectWriter writer = map.writer().withDefaultPrettyPrinter();
        String requestBody = writer.writeValueAsString(soul);

        mockMvc.perform(put("/soul/2")
                .contentType(MEDIA)
                .content(requestBody))
                .andExpect(status().isOk());
    }

    @Test
    public void testDelete() throws Exception {
        mockMvc.perform(delete("/soul/1"))
                .andExpect(status().isBadRequest());
    }

}
