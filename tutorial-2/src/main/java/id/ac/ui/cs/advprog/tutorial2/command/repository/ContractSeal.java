package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell latestSpell;
    private boolean flag;

    public ContractSeal() {
        spells = new HashMap<>();
        this.flag = true;
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // TODO: Complete Me
        this.latestSpell = spells.get(spellName);
        this.latestSpell.cast();
        this.flag = true;
    }

    public void undoSpell() {
        // TODO: Complete Me
        // case when latestSpell is empty
        if(this.latestSpell != null && this.flag == true) {
            this.latestSpell.undo();
            this.flag = false;
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
