package id.ac.ui.cs.advprog.tutorial2.template.core;

import java.util.ArrayList;
import java.util.List;

public abstract class SpiritInQuest {

    public List attackPattern() {
        List list = new ArrayList();
        // TODO: Complete Me
        list.add(summon());
        list.add(getReady());

        list.add(attackWithBuster());
        list.add(attackWithQuick());
        list.add(attackWithArts());
        list.add(attackWithSpecialSkill());

        String Buff = buff();
        if(Buff.indexOf("Buster") != -1) {
            list.add(2,Buff);
        }
        if(Buff.indexOf("Quick") != -1) {
            list.add(3,Buff);
        }
        if(Buff.indexOf("Arts") != -1) {
            list.add(4,Buff);
        }
        if(Buff.indexOf("Special Skill") != -1) {
            list.add(5,Buff);
        }

        return list;
    }


    public String summon() {
        return "Summon a Spirit...";
    }

    public String getReady() {
        return "Spirit ready to enter the Quest";
    }

    protected abstract String buff();

    protected abstract String attackWithBuster();

    protected abstract String attackWithQuick();

    protected abstract String attackWithArts();

    protected abstract String attackWithSpecialSkill();
}
