package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {

    private String wish;
    private static HolyWish _instance;

    // TODO complete me with any Singleton approach
    // lazy implementation
    private HolyWish(){}

    public static HolyWish getInstance() {
        if(_instance == null) {
            _instance = new HolyWish();
        }
        return _instance;
    }

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
}
