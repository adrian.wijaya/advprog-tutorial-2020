package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill;

public class ThousandYearsOfPain implements Skill {

    @Override
    public String getName() {
        // TODO fix me
        return "ThousandYearsOfPain";
    }

    @Override
    public String getDescription() {
        // TODO fix me
        return "Feel the pain";
    }
}
