package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import java.util.*;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

//@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    //@Mock
    private AcademyRepository academyRepository;

    //@InjectMocks
    private AcademyServiceImpl academyService;

    // TODO create tests
    @BeforeEach
    public void setUp() {
        // TODO setup me
        academyRepository = new AcademyRepository();
        academyService = new AcademyServiceImpl(academyRepository);
    }


    @Test
    public void checkKnightExist() {
        assertEquals(null,academyService.getKnight());
    }

    @Test
    public void checkKnightAcademies() {
        // TODO create test
        assertTrue(academyService.getKnightAcademies().get(0) instanceof LordranAcademy);
    }

    @Test
    public void checkKnightProducedorNot() {
        assertTrue(academyRepository.getKnightAcademyByName("Lordran").getKnight("metal cluster") instanceof MetalClusterKnight);
    }



}
