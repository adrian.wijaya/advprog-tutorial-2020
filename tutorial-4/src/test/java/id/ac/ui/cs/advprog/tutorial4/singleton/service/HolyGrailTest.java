package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.service.HolyGrail;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {
    private Class<?> holyGrailClass;
    private HolyGrail holyGrail;

    @BeforeEach
    public void setUp() throws Exception {
        holyGrailClass = Class.forName(HolyGrail.class.getName());
        holyGrail = mock(HolyGrail.class);
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(holyGrailClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertTrue(check);
    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
        // TODO create test
        holyGrail.makeAWish("AProg ku A");
        verify(holyGrail,times(1)).makeAWish("AProg ku A");
    }

    @Test
    public void testWish(){
        // TODO create test
        holyGrail.makeAWish("TBA ku dapat A");
        verify(holyGrail,times(1)).makeAWish("TBA ku dapat A");

    }


}
