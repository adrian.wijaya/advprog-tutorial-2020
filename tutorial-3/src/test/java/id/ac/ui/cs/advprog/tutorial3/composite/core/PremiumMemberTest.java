package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals( "Wati",member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals( "Gold Merchant",member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member dummy = new OrdinaryMember("zzz","slave");
        member.addChildMember(dummy);
        assertEquals(1,member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member dummy = new PremiumMember("zzz","slave");
        member.addChildMember(dummy);
        member.removeChildMember(dummy);
        assertEquals(0,member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member dummy = new PremiumMember("zzz","slave");
        Member dummy2 = new PremiumMember("aaa","slave");
        Member dummy3 = new OrdinaryMember("bbb","slave");
        Member dummy4 = new PremiumMember("ccc","slave");

        member.addChildMember(dummy);
        member.addChildMember(dummy2);
        member.addChildMember(dummy3);
        member.addChildMember(dummy4);
        assertEquals(3,member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member dummyMaster = new PremiumMember("Owner", "Master");
        Member dummy = new PremiumMember("zzz","slave");
        Member dummy2 = new PremiumMember("aaa","slave");
        Member dummy3 = new OrdinaryMember("bbb","slave");
        Member dummy4 = new PremiumMember("ccc","slave");

        dummyMaster.addChildMember(dummy);
        dummyMaster.addChildMember(dummy2);
        dummyMaster.addChildMember(dummy3);
        dummyMaster.addChildMember(dummy4);
        assertEquals(4,dummyMaster.getChildMembers().size());
    }
}
