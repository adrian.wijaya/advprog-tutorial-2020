package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    //TODO: Complete me
    private String name,role;
    private List<Member> childMember;

    public PremiumMember(String name, String role) {
        this.name = name;
        this.role = role;
        this.childMember = new ArrayList<Member>();
    }

    public String getName() {
        return this.name;
    }
    public String getRole() {
        return this.role;
    }
    public void addChildMember(Member member) {
        if(!this.role.equals("Master") && this.childMember.size() >= 3){
            return;
        }
        this.childMember.add(member);
    }
    public void removeChildMember(Member member) {
        String childName = member.getName();
        String childRole = member.getRole();
        // beware runtime error
        for(int i =0; i <(int)childMember.size(); i++) {
            Member m = childMember.get(i);
            if(m.getName().equals(childName) && m.getRole().equals(childRole)) {
                childMember.remove(i);
                break;
            }
        }
    }
    public List<Member> getChildMembers(){
        return this.childMember;
    }

}
