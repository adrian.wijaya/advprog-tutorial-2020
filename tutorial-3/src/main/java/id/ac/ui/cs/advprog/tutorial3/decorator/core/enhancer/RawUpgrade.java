package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;
    private int enhance;

    public RawUpgrade(Weapon weapon) {
        Random rand = new Random();
        this.enhance = 5 + rand.nextInt(6);
        this.weapon= weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        //TODO: Complete me
        if(this.weapon == null) return enhance;
        return this.weapon.getWeaponValue() + enhance;
    }

    @Override
    public String getDescription() {
        //TODO: Complete me
        return "RawUpgrade " +  this.weapon.getDescription();
    }
}
